﻿using System;
using System.Collections.Generic;
using System.Text;
using TestTrial.Models;
using TestTrial.Services;
using TravelRepublic.FlightCodingTest;

namespace TestTrial
{
    public class FlightBoard : IFlightBoard
    {
        public IFlightBuilder flightBuilder { get; }
        private IFlightReadingService flightReadingService { get; }
        private IFilterServices filterServices { get; }
        private IList<IFlight> flights { get; set; }
        private int tableWidth = 230;
        public FlightBoard(IFlightBuilder flightBuilder, IFlightReadingService flightReadingService, IFilterServices filterServices)
        {
            this.flightBuilder = flightBuilder;
            this.flightReadingService = flightReadingService;
            this.filterServices = filterServices;
        }

        public void DisplayFlightBoard()
        {
            flights = filterServices.ApplyAllFilters(flightBuilder.GetFlights());
            DisplayBoard();

        }


       private void DisplayBoard()
        {
            Console.Clear();
            PrintLine();
            PrintRow("Depart", "Arrival", "Middle Segments");
            PrintLine();
            foreach (var flight in flights)
            {
                flightReadingService.SetUpFlight(flight);
                PrintRow(flightReadingService.GetSource(), flightReadingService.GetDestination(), flightReadingService.GetFullJourney());
            }
            PrintLine();
            Console.ReadLine();
        }

        private void PrintLine()
        {
            Console.WriteLine(new string('-', tableWidth));
        }
        private void PrintRow(params string[] columns)
        {
            int widthStandard = (tableWidth - columns.Length) / (columns.Length * 2);
            int widthLarge = (tableWidth - widthStandard * 2);
            string row = $"| {AlignCentre(columns[0], widthStandard)} | {AlignCentre(columns[1], widthStandard)} | {AlignCentre(columns[2], widthLarge)} |";


            Console.WriteLine(row);
        }

        private string AlignCentre(string text, int width)
        {
            text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

            if (string.IsNullOrEmpty(text))
            {
                return new string(' ', width);
            }
            else
            {
                return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
            }
        }

    }
}
