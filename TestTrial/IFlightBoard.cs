﻿using System;
using System.Collections.Generic;
using System.Text;
using TravelRepublic.FlightCodingTest;

namespace TestTrial
{
    interface IFlightBoard
    {
        IFlightBuilder flightBuilder { get; }
        void DisplayFlightBoard();
    }
}
