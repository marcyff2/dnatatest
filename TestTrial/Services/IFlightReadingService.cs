﻿using System;
using System.Collections.Generic;
using System.Text;
using TestTrial.Models;

namespace TestTrial.Services
{
    public interface IFlightReadingService
    {
        void SetUpFlight(IFlight flight);
        string GetSource();
        string GetDestination();
        string GetFullJourney();
    }
}
