﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestTrial.Models;

namespace TestTrial.Services
{
    public class filterServices : IFilterServices
    {

        public IList<IFlight> ApplyAllFilters(IList<IFlight> flights)
        {
            return FilterFlightsThatHaveDeparted(FilterFlightsThatHaveLongWaits(FilterFlightsThatConnectionsArePast(flights)));
        }
        public IList<IFlight> FilterFlightsThatHaveDeparted(IList<IFlight> flights)
        {
            return flights.Where(x => x.Segments[0].DepartureDate >= DateTime.Now).ToList();
         
        }

        public IList<IFlight> FilterFlightsThatHaveLongWaits(IList<IFlight> flights)
        {
            var result = new List<IFlight>();
            foreach (var flight in flights)
            {
                var isLongWait = false;
                var previousArrival = flight.Segments[0].ArrivalDate;
                foreach(var segment in flight.Segments)
                {
                    if((segment.DepartureDate - previousArrival).Hours>2)
                    {
                        isLongWait = true;
                        break;
                    }
                }
                if (!isLongWait)
                    result.Add(flight);
            }
            return result;
        }

        public IList<IFlight> FilterFlightsThatConnectionsArePast(IList<IFlight> flights)
        {
            var result = new List<IFlight>();
            foreach (var flight in flights)
            {
                if (flight.Segments.FirstOrDefault(s => s.ArrivalDate < s.DepartureDate) == null)
                    result.Add(flight);
            }
            return result;
        }
    }
}
