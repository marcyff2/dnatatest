﻿using System;
using System.Collections.Generic;
using System.Text;
using TestTrial.Models;

namespace TestTrial.Services
{
    public interface IFilterServices
    {
        public IList<IFlight> FilterFlightsThatHaveDeparted(IList<IFlight> flights);
        public IList<IFlight> FilterFlightsThatConnectionsArePast(IList<IFlight> flights);
        public IList<IFlight> FilterFlightsThatHaveLongWaits(IList<IFlight> flights);
        public IList<IFlight> ApplyAllFilters(IList<IFlight> flights);
    }
}
