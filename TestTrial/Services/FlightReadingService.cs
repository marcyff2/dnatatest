﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestTrial.Models;

namespace TestTrial.Services
{
    public class FlightReadingService : IFlightReadingService
    {
        public IFlight flight { get; set; }

        public FlightReadingService()
        {
        }

        public void SetUpFlight(IFlight flight)
        {
            this.flight = flight;
        }

        public string GetDestination()
        {

            return flight.Segments.FirstOrDefault().ArrivalDate.ToString("dd/MM HH:mm");
        }

        public string GetFullJourney()
        {
            var fullJourney = "";
            foreach(var segment in flight.Segments)
            {
                fullJourney += $"D:{segment.DepartureDate.ToString("dd/MM HH:mm")} - A:{segment.DepartureDate.ToString("dd/MM HH:mm")} | ";
            }
            return fullJourney.Substring(0, fullJourney.Length - 3);
        }

        public string GetSource()
        {
            return flight.Segments.FirstOrDefault().DepartureDate.ToString("dd/MM HH:mm");
        }

    }
}
