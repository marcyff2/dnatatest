﻿using System;
using System.Collections.Generic;
using TestTrial.Models;

namespace TravelRepublic.FlightCodingTest
{
    public interface IFlightBuilder
    {
        public IList<IFlight> GetFlights();

    }
}