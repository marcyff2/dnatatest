﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTrial.Models
{
    public class Segment :ISegment 
    {
        public DateTime DepartureDate { get; set; }
        public DateTime ArrivalDate { get; set; }
    }
}
