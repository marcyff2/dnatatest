﻿using System.Collections.Generic;

namespace TestTrial.Models
{
    public interface IFlight
    {
        public IList<Segment> Segments { get; set; }

    }
}