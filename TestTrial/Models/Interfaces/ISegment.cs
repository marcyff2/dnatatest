﻿using System;

namespace TestTrial.Models
{
    public interface ISegment
    {
        DateTime DepartureDate { get; set; }
        DateTime ArrivalDate { get; set; }
    }
}