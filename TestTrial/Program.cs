﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestTrial.Models;
using TestTrial.Services;
using TravelRepublic.FlightCodingTest;

namespace TestTrial
{
    class Program
    {
      private static void Main(string[] args)
        {
            var services = new ServiceCollection();
            services.AddSingleton<IFlightBuilder, FlightBuilder>();
            services.AddSingleton<IFlightBoard, FlightBoard>();
            services.AddSingleton<IFilterServices, filterServices>();
            services.AddSingleton<IFlightBoard>(x =>new FlightBoard(x.GetRequiredService<IFlightBuilder>(), x.GetRequiredService<IFlightReadingService>(), x.GetRequiredService<IFilterServices>()));
            services.AddSingleton<IFlightReadingService, FlightReadingService>();
            services.AddTransient<IFlight, Flight>();
            services.AddTransient<ISegment, Segment>();
            services.AddTransient<IEnumerable<ISegment>, List<Segment>>();
            var serviceProvider = services.BuildServiceProvider();
            MainCall(serviceProvider);
            
        }

        private static void  MainCall(IServiceProvider serviceProvider)
        {
                var flightBoard = serviceProvider.GetRequiredService<IFlightBoard>();
                flightBoard.DisplayFlightBoard();
                
    
        }
    }
}
