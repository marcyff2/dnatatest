using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TestTrial.Models;
using TestTrial.Services;
using Xunit;

namespace TestTrial.Test
{
    public class FilterServicesTest
    {
        [Fact]
        public void FilterFlightsThatHaveDeparted()
        {
            IList<IFlight> mockFlights = new List<IFlight>();

            var mockSegments = new List<Segment>();
            mockSegments.Add(new Segment { DepartureDate = DateTime.Now.AddDays(-1), ArrivalDate = DateTime.Now.AddDays(2) });
            mockFlights.Add(new Flight { Segments = mockSegments });
            var service = new filterServices();
            var result = service.FilterFlightsThatHaveDeparted(mockFlights);
            Assert.Equal(0, result.Count);

        }

        [Fact]
        public void FiterFlightsThatConnectionsArePast()
        {
            IList<IFlight> mockFlights = new List<IFlight>();

            var mockSegments = new List<Segment>();
            mockSegments.Add(new Segment { DepartureDate = DateTime.Now.AddDays(1), ArrivalDate = DateTime.Now });
            mockSegments.Add(new Segment { DepartureDate = DateTime.Now.AddDays(-1), ArrivalDate = DateTime.Now.AddDays(1).AddHours(3) });
            mockFlights.Add(new Flight { Segments = mockSegments });
            var service = new filterServices();
            var result = service.FilterFlightsThatConnectionsArePast(mockFlights);
            Assert.Equal(0, result.Count);

        }

        [Fact]
        public void FilterFlightsThatHaveLongWaits()
        {
            IList<IFlight> mockFlights = new List<IFlight>();

            var mockSegments = new List<Segment>();
            mockSegments.Add(new Segment { DepartureDate = DateTime.Now, ArrivalDate = DateTime.Now.AddDays(1) });
            mockSegments.Add(new Segment { DepartureDate = DateTime.Now.AddDays(1).AddHours(3), ArrivalDate = DateTime.Now.AddDays(2) });
            mockFlights.Add(new Flight { Segments = mockSegments });
            var service = new filterServices();
            var result = service.FilterFlightsThatHaveLongWaits(mockFlights);
            Assert.Equal(0, result.Count);

        }
    }
}
